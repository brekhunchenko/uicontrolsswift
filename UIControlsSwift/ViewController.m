//
//  ViewController.m
//  UIControlsSwift
//
//  Created by Yaroslav Brekhunchenko on 10/12/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import "ViewController.h"
#import "UIControlsSwift-Swift.h"

@interface ViewController () < TwicketSegmentedControlDelegate >

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    TwicketSegmentedControl* segmentedControl = [[TwicketSegmentedControl alloc] initWithFrame:CGRectMake(5.0f, 300.0f, self.view.frame.size.width - 10.0f, 40.0f)];
    [segmentedControl setSegmentItems:@[@"First", @"Second", @"Third"]];
    segmentedControl.delegate = self;
    [segmentedControl moveTo:2 animated:NO];
    [self.view addSubview:segmentedControl];
}

#pragma mark - TwicketSegmentedControl Delegate

- (void)didSelect:(NSInteger)segmentIndex {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

@end
