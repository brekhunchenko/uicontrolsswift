//
//  AppDelegate.h
//  UIControlsSwift
//
//  Created by Yaroslav Brekhunchenko on 10/12/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

